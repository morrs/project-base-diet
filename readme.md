# Website project base - diet
A Grunt and Bower powered HTML, CSS(using [SMACSS](http://smacss.com/)) and JS website development framework integrating jQuery and SASS as well as incorporating the [Bourbon mixin library](http://bourbon.io/) and [grid system](http://neat.bourbon.io/).  


## Installation
* Clone the project base to your working directory.
* Install npm (sudo npm install)
* Run **bower install** to jQuery(and any other required JavaScript libraries, see **extending the framework**).
* Run **grunt** to build the initial project stucture.

## Use
* Run **grunt watch** to monitor the project for changes.

## Extending the framework
As mentioned in the installation other JavaScript libraries can be added to enhance the functionality of this framework.

* Other packages required will need to be defined in the **bower.json** before running the **bower install** command.
* These will also need to be added to the **gruntfile.js** in the project root under the **concat** section with each additional library defined.


## Why you jQuery?
I find that most of my projects tend to involve the use of this JavaScript library and so it is bundled in as standard. However not all projects require it. If this is the case then simply:-

* Skip the **bower install** command on installation.
* Remove references to the scripts file in the **_includes/footer.html** file.
* Remove the **js** tasks from the **watch** task in the **gruntfile.js** in the project root.
* Run **grunt** followed by **grunt watch** as standard.

module.exports = function(grunt){

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        htmlhint: {
		    build: {
		        options: {
		            'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': true,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'style-disabled': true
		        },
		        src: ['*.html']
		    }
		},
		htmlmin: {    
			dist: {
				options: {                                 
					removeComments: true,
					collapseWhitespace: true,
				},
				files: [{
        			expand: true,                  
        			cwd: '.',                   
        			src: ['*.html'],
        			dest: 'build/' 
      			}]
			}                                 
		},
		sass: {                              
		    build: {
		    	options: {                       
					style: 'compressed',
					sourcemap: true,
					quiet: true,
					debugInfo: true
				},
				files: {
					'build/assets/css/styles.css': 'assets/sass/main.scss'
				}
		    }
  		},
		autoprefixer: {
		    single_file: {		 
		      src: 'build/assets/css/styles.css',
		      dest: 'build/assets/css/styles.css'
		    }
	  	},
		concat: {
	        js : {
	            src : [ 'assets/bower_components/jquery/jquery.js', 'assets/js/scripts.js'],
	            dest : 'build/assets/js/scripts.js'
	        }
	    },
	    uglify: {
		    build: {
		        files: {
		            'build/assets/js/scripts.js': ['build/assets/js/scripts.js'],
		            'build/assets/js/modernizr.js': ['assets/bower_components/modernizr/modernizr.js']
		        }
		    }
		},
		watch: {
			html: {
                files: [ '*.html' ],
                tasks: [ 'htmlhint', 'htmlmin', 'notify:watch' ]
            },
			scss: {
                files: [ 'assets/sass/**/*.scss' ],
                tasks: [ 'sass', 'autoprefixer', 'notify:watch' ]
            },
            js: {
                files: [ 'assets/js/*.js' ],
                tasks: [ 'concat', 'uglify', 'notify:watch' ]
            },
            options: {	        
		        livereload: true
	        }
        },
        concurrent: {
		    targets: ['htmlhint', 'htmlmin', 'sass', 'autoprefixer', 'concat', 'uglify']
		},
        notify: {
			build: {
				options: {
					title: 'Site build',
					message: 'Build complete'
				}
			},
			watch: {
				options: {
					title: 'Success',
					message: 'File/s built'
				}
			}
	    }
    });

	grunt.registerTask( 'default', [
		'concurrent',
        'notify:build'
    ]);

};